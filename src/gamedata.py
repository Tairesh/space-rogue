import os
import json

traits = {}
professions = {}
races = {}


def init():
    data_dir = os.path.join('res', 'data')
    for file in os.listdir(data_dir):
        with open(os.path.join(data_dir, file)) as fp:
            d = json.load(fp)
            if d['file'] == 'traits':
                for t in d['data']:
                    traits[t['id']] = t
            elif d['file'] == 'professions':
                for p in d['data']:
                    professions[p['id']] = p
            elif d['file'] == 'races':
                for r in d['data']:
                    races[r['id']] = r

    print(f"Loaded JSON-data: {len(races)} races, {len(professions)} professions, {len(traits)} traits")
