import json
import os
import consts

width = 800
height = 600
show_fps = True
fullscreen = False


def as_dict():
    return {
        'width': width,
        'height': height,
        'show_fps': show_fps,
        'fullscreen': fullscreen,
    }


def rewrite_file(settings_dict=None):
    if not settings_dict:
        settings_dict = as_dict()
    with open(consts.SETTINGS_FILENAME, 'w+') as file:
        file.write(json.dumps(settings_dict))


def validate(settings_dict):
    for key in as_dict().keys():
        if key not in settings_dict:
            return False

    if not 640 <= settings_dict['width'] <= 4096:
        return False
    if not 480 <= settings_dict['height'] <= 3072:
        return False

    return True


def correct(settings_dict):
    if 'width' not in settings_dict:
        print(f"Settings file have not key \"width\", using default: {width}")
        settings_dict['width'] = width
    elif settings_dict['width'] < 640:
        print('Window width must be bigger than 640, using minimum possible value')
        settings_dict['width'] = 640
    elif settings_dict['width'] > 4096:
        print('Window width must be less than 4096, using maximum possible value')
        settings_dict['width'] = 4096

    if 'height' not in settings_dict:
        print(f"Settings file have not key \"height\", using default: {height}")
        settings_dict['height'] = height
    elif settings_dict['height'] < 480:
        print('Window height must be bigger than 480, using minimum possible value')
        settings_dict['height'] = 480
    elif settings_dict['height'] > 3072:
        print('Window height must be less than 3072, using maximum possible value')
        settings_dict['height'] = 3072

    if 'show_fps' not in settings_dict:
        print(f"Settings file have not key \"show_fps\", using default: {show_fps}")
        settings_dict['show_fps'] = show_fps

    if 'fullscreen' not in settings_dict:
        print(f"Settings file have not key \"fullscreen\", using default: {fullscreen}")
        settings_dict['fullscreen'] = fullscreen

    return settings_dict


def load():
    """
     Returns dict with game settings
    """
    if os.path.exists(consts.SETTINGS_FILENAME):
        with open(consts.SETTINGS_FILENAME) as file:
            try:
                settings_dict = json.load(file)
                if not validate(settings_dict):
                    print('Invalid settings, replacing settings.json...')
                    settings_dict = correct(settings_dict)
                    rewrite_file(settings_dict)
                else:
                    print('Loaded settings.json')
                return settings_dict
            except json.decoder.JSONDecodeError:
                print('Invalid settings.json, creating new file...')
                rewrite_file()
                return as_dict()
    else:
        print('Creating new file settings.json...')
        rewrite_file()
        return as_dict()


def save(data):
    """
     Save new game settings
    """
    print('Rewriting file settings.json...')
    rewrite_file(data)


def init():
    global width, height, show_fps, fullscreen
    settings_dict = load()
    width, height, show_fps, fullscreen = (settings_dict['width'], settings_dict['height'],
                                           settings_dict['show_fps'], settings_dict['fullscreen'])
