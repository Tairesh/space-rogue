from pygame import Rect
import engine


screen_width = 800
screen_height = 600
side_panel_width = 300
main_window = Rect(0, 0, screen_width, screen_height)
main_window_wo_side_panel = Rect(0, 0, screen_width-side_panel_width, screen_height)
side_panel = Rect(screen_width-side_panel_width, 0, side_panel_width, screen_height)


def calc():
    global screen_width, screen_height, side_panel_width, main_window, main_window_wo_side_panel, side_panel

    screen_width, screen_height = engine.window.get_size()
    side_panel_width = min(300, max(screen_width // 4, 200))
    main_window = Rect(0, 0, screen_width, screen_height)
    main_window_wo_side_panel = Rect(0, 0, screen_width-side_panel_width, screen_height)
    side_panel = Rect(screen_width-side_panel_width, 0, side_panel_width, screen_height)
    print(f"Sizes calculated: ({screen_width}x{screen_height}), side panel: {side_panel_width}px")
