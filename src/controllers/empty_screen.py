from . import ControllerView
import engine
import colors
import sizes
import pygame
from drawable.buttons import Button
from drawable.inputs import TextInput, Selector
import controllers.mainmenu


class EmptyScreen(ControllerView):

    def __init__(self):
        super().__init__()
        self.rgb = colors.Lime
        self.rgb_reverse = False
        self.black_screen = True
        btn = Button((sizes.screen_width//2, sizes.screen_height//2, 'center', 'center'), "Test", groups=(self.entities, ))
        inp = TextInput((sizes.screen_width//2, btn.rect.bottom+5, 'center', 'top'), "textAsdsaAsd", 200, groups=(self.entities, ))
        sel = Selector((sizes.screen_width//2, inp.rect.bottom+5, 'center', 'top'), {
            0: 'asd',
            1: 'dsa',
            2: 'foo',
            3: 'bar'
        }, 0, 200, groups=(self.entities, ))

    def draw(self, surface: pygame.Surface):
        engine.window.fill(colors.Black if self.black_screen else self.rgb)
        super().draw(surface)

    def call(self, event: pygame.event.Event):
        super().call(event)
        if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
            self.black_screen = not self.black_screen
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            engine.change_controller(controllers.mainmenu.MainMenu())

    def tick(self):
        super().tick()
        if self.black_screen:
            return
        r, g, b = self.rgb
        if self.rgb_reverse:
            if r > 0:
                r -= 1
            elif g > 0:
                g -= 1
            elif b > 0:
                b -= 1
            else:
                self.rgb_reverse = False
        else:
            if r < 255:
                r += 1
            elif g < 255:
                g += 1
            elif b < 255:
                b += 1
            else:
                self.rgb_reverse = True
        self.rgb = r, g, b
