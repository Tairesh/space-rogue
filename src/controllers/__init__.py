import pygame
from drawable import Sprite
from typing import Optional


class ControllerView:

    def __init__(self):
        # TODO: singletone controllers
        # TODO: entities focus map
        self.entities = pygame.sprite.Group()
        self.focused_entity: Optional[Sprite] = None

    def call(self, event: pygame.event.Event):
        """
        Called with any pygame events (except system ones as Quit and WindowResize)
        :param event: pygame.event.Event
        :return:
        """
        if event.type == pygame.MOUSEMOTION:
            for entity in self.entities:
                if entity.rect.collidepoint(*event.pos):
                    entity.on_hover()
                else:
                    entity.off_hover()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            for entity in self.entities:
                if entity.rect.collidepoint(*event.pos):
                    entity.on_pressed()
            if self.focused_entity and not self.focused_entity.rect.collidepoint(*event.pos):
                self.focused_entity.off_focused()
                self.focused_entity = None
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            for entity in self.entities:
                entity.off_pressed()
                if entity.rect.collidepoint(*event.pos):
                    entity.on_click(event.pos)
                    if entity.focusable:
                        self.focused_entity = entity
                        self.focused_entity.on_focused()
        elif event.type == pygame.KEYDOWN and not event.mod & pygame.KMOD_ALT and not event.mod & pygame.KMOD_CTRL:
            for entity in [e for e in self.entities if e.key_activated]:
                if event.key == entity.key:
                    entity.on_pressed()
        elif event.type == pygame.KEYUP and not event.mod & pygame.KMOD_ALT and not event.mod & pygame.KMOD_CTRL:
            for entity in [e for e in self.entities if e.key_activated]:
                if event.key == entity.key:
                    entity.off_pressed()
                    entity.on_click(None)

        if self.focused_entity and self.focused_entity.events_subscribe_on_focused:
            self.focused_entity.call(event)

    def draw(self, surface: pygame.Surface):
        self.entities.draw(surface)

    def on_resize(self):
        """
        Calls when window resized
        :return:
        """
        # TODO: set positions of entities automatically

    def tick(self):
        """
        Called 60(fps) times in a second
        :return:
        """
        self.entities.update()
