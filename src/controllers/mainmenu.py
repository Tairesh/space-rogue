from . import ControllerView
from drawable.buttons import Button
import sizes
import engine
import pygame
from controllers.empty_screen import EmptyScreen


class MainMenu(ControllerView):

    def __init__(self):
        super().__init__()
        btn_exit = Button((sizes.side_panel_width//2, sizes.screen_height-50, 'center', 'bottom'),
                          '[x] Exit', False,
                          lambda btn: exit(0), 150, pygame.K_x,
                          (self.entities, ))
        btn_test = Button((sizes.side_panel_width//2, btn_exit.rect.top-10, 'center', 'bottom'),
                          '[t] Test screen', False,
                          lambda btn: engine.change_controller(EmptyScreen()), 150, pygame.K_t,
                          (self.entities, ))
