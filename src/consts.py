
VERSION = '0.0.1-dev'

WINDOW_TITLE = 'Space Age'
WINDOW_ICON = 'res/img/icon.png'
FPS = 60
SHOW_FPS = True
SETTINGS_FILENAME = 'settings.json'
TILESET_FILE = 'res/img/tileset.png'
TILESET_PIXEL_SIZE = 10
TILESET_MAP = [
    ['*', '┼', '┌', '│', '^', '┬', 'door_open', 'door'],
    ['seat', 'radiation', 'machine', 'frame', 'frame_left_top', 'frame_top', None, None],
    ['human', 'octopus', None, None, None, None, None, None],
    ['stars', 'sharp_floor', 'metal_floor', 'floor', None, None, None, None],
    ['star', 'planet', None, None, None, None, None, ' '],
]
