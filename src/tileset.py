from consts import TILESET_FILE, TILESET_MAP, TILESET_PIXEL_SIZE
import pygame


raw_tiles = {}
prepared_tiles = {}


def init():
    sheet = pygame.image.load(TILESET_FILE).convert_alpha()
    for i, row in enumerate(TILESET_MAP):
        for j, ch in enumerate(row):
            if ch is None:
                continue
            x, y = j * TILESET_PIXEL_SIZE, i * TILESET_PIXEL_SIZE

            image = pygame.Surface((TILESET_PIXEL_SIZE, TILESET_PIXEL_SIZE), pygame.SRCALPHA, 32).convert_alpha()
            image.blit(sheet, (0, 0), (x, y, TILESET_PIXEL_SIZE, TILESET_PIXEL_SIZE))
            raw_tiles[ch] = image

    raw_tiles['┐'] = pygame.transform.rotate(raw_tiles['┌'], -90)
    raw_tiles['┘'] = pygame.transform.rotate(raw_tiles['┌'], 180)
    raw_tiles['└'] = pygame.transform.rotate(raw_tiles['┌'], 90)
    raw_tiles['─'] = pygame.transform.rotate(raw_tiles['│'], 90)
    raw_tiles['├'] = pygame.transform.rotate(raw_tiles['┬'], 90)
    raw_tiles['┤'] = pygame.transform.rotate(raw_tiles['┬'], -90)
    raw_tiles['┴'] = pygame.transform.rotate(raw_tiles['┬'], 180)

    print(f"Tileset loaded: {TILESET_FILE}")


def get_tile(ch, fg=None, bg=None, zoom=1):
    if ch not in raw_tiles:
        print(f"WARNING: {ch} not in tileset!")
    key = f"{ch}_{fg}_{bg}_{zoom}"
    if key not in prepared_tiles:
        size = (round(TILESET_PIXEL_SIZE * zoom), round(TILESET_PIXEL_SIZE * zoom))
        surface = pygame.Surface(size, pygame.SRCALPHA, 32).convert_alpha()
        if bg:
            surface.fill(bg)
        render = raw_tiles[ch].copy() if ch in raw_tiles else raw_tiles[' ']
        if fg:
            render.fill(fg, None, pygame.BLEND_RGB_MULT)
        if not zoom == 1:
            render = pygame.transform.scale(render, size)
        surface.blit(render, (0, 0))
        prepared_tiles[key] = surface
    return prepared_tiles[key]
