import pygame
from typing import Tuple, Optional, Iterable


def octagon(w, h):
    return (5, 0), (w-6, 0), (w, 5), (w, h-5), (w-6, h), (5, h), (0, h-5), (0, 5)


class Sprite(pygame.sprite.DirtySprite):

    focusable = False
    focusable_view = False
    hoverable = False
    pressable = False
    disableable = False
    events_subscribe_on_focused = False
    key_activated = False

    def __init__(self, pos: Tuple[int, int, Optional[str], Optional[str]], groups: Iterable = ()):
        self.hovered = False
        self.pressed = False
        self.disabled = False
        self.focused = False
        self.renders = self._render_all_states()
        self.image: pygame.Surface = self._get_actual_render()
        self.pos: Pos = Pos(self, *pos) if pos else None
        self.rect = pygame.Rect(self.pos.left, self.pos.top, self.image.get_width(), self.image.get_height())
        super().__init__(*groups)

    def _get_actual_render(self) -> pygame.Surface:
        if self.disabled and 'disabled' in self.renders:
            return self.renders['disabled']
        elif self.pressed and 'pressed' in self.renders:
            return self.renders['pressed']
        elif self.focused and 'focused' in self.renders:
            return self.renders['focused']
        elif self.hovered and 'hovered' in self.renders:
            return self.renders['hovered']
        else:
            return self.renders['default']

    def update(self):
        if self.dirty > 0:
            self.image = self._get_actual_render()
            if self.dirty < 2:
                self.dirty = 0

    def _render_all_states(self):
        renders = {
            'default': self._render_state('default'),
        }
        if self.hoverable:
            renders['hovered'] = self._render_state('hovered')
        if self.pressable:
            renders['pressed'] = self._render_state('pressed')
        if self.disableable:
            renders['disabled'] = self._render_state('disabled')
        if self.focusable_view:
            renders['focused'] = self._render_state('focused')
        return renders

    def _render_state(self, state) -> pygame.Surface:
        raise Exception(f"`_render_state` method not overriden in {self.__class__.__name__}")

    def _re_render(self):
        self.renders = self._render_all_states()
        if self.dirty == 0:
            self.dirty = 1

    def on_hover(self):
        if self.hovered or self.disabled:
            return
        self.hovered = True
        if self.dirty == 0:
            self.dirty = 1

    def off_hover(self):
        if not self.hovered or self.disabled:
            return
        self.hovered = False
        if self.dirty == 0:
            self.dirty = 1

    def on_pressed(self):
        if self.pressed or self.disabled:
            return
        self.pressed = True
        if self.dirty == 0:
            self.dirty = 1

    def off_pressed(self):
        if not self.pressed or self.disabled:
            return
        self.pressed = False
        if self.dirty == 0:
            self.dirty = 1

    def on_focused(self):
        if self.focused or self.disabled:
            return
        self.focused = True
        if self.dirty == 0:
            self.dirty = 1

    def off_focused(self):
        if not self.focused or self.disabled:
            return
        self.focused = False
        if self.dirty == 0:
            self.dirty = 1

    def on_click(self, pos):
        pass

    def call(self, event: pygame.event.Event):
        pass


class Pos:
    def __init__(self, owner: Sprite, x: int, y: int, anchor_x='left', anchor_y='top'):
        self.owner = owner
        self.x = x
        self.y = y
        self.anchor_x = anchor_x
        self.anchor_y = anchor_y
        self.left, self.right, self.top, self.bottom = self._calc()

    def set(self, x, y, anchor_x=None, anchor_y=None):
        self.x = x
        self.y = y
        if anchor_x is not None:
            self.anchor_x = anchor_x
        if anchor_y is not None:
            self.anchor_y = anchor_y
        self.left, self.right, self.top, self.bottom = self._calc()

    def _calc(self):
        if self.anchor_x == 'left':
            left = self.x
        elif self.anchor_x == 'center':
            left = self.x - self.owner.image.get_width()//2
        elif self.anchor_x == 'right':
            left = self.x - self.owner.image.get_width()
        else:
            raise Exception("Invalid anchor_x value")

        if self.anchor_y == 'top':
            top = self.y
        elif self.anchor_y == 'center':
            top = self.y - self.owner.image.get_height()//2
        elif self.anchor_y == 'bottom':
            top = self.y - self.owner.image.get_height()
        else:
            raise Exception("Invalid anchor_y value")

        right = left + self.owner.image.get_width()
        bottom = top + self.owner.image.get_height()
        return left, right, top, bottom
