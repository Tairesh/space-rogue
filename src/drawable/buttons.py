from . import Sprite, octagon
import pygame
import fonts
import colors
from typing import Callable, Tuple, Optional, Iterable


class Button(Sprite):

    focusable = True
    focusable_view = True
    hoverable = True
    pressable = True
    disableable = True

    @property
    def key_activated(self):
        return self.key is not None

    def __init__(self, pos: Tuple[int, int, Optional[str], Optional[str]], text: str, focusable: bool = False,
                 action: Callable = lambda btn: None, width: int = None, key: int = None, groups: Iterable = ()):
        self.text: str = text
        self.focusable: bool = focusable
        self.action: Callable = action
        self.width = width
        self.key = key
        super().__init__(pos, groups)

    def _render_button(self, text_color, background_color, border) -> pygame.Surface:
        text_surface = fonts.mono.render(self.text, True, text_color)
        w = self.width if self.width is not None else text_surface.get_width()+10
        s = pygame.Surface((w, text_surface.get_height()+10), pygame.SRCALPHA, 32).convert_alpha()
        pol = octagon(s.get_width()-1, s.get_height()-1)
        pygame.draw.polygon(s, background_color, pol, 0)
        if border:
            pygame.draw.polygon(s, text_color, pol, border)
        s.blit(text_surface, (w//2-text_surface.get_width()//2, 5))
        return s

    def _render_state(self, state) -> pygame.Surface:
        if state == 'default':
            return self._render_button(colors.LightSpaceBlue, colors.Black, True)
        elif state == 'focused':
            return self._render_button(colors.White, colors.LightSpaceBlue, True)
        elif state == 'hovered':
            return self._render_button(colors.White, colors.LightSpaceBlue, False)
        elif state == 'pressed':
            return self._render_button(colors.White, colors.SpaceBlue, True)
        elif state == 'disabled':
            return self._render_button(colors.LightGray, colors.DarkGray, True)
        else:
            raise Exception(f"Unknown state `{state}` for class {self.__class__.__name__}")

    def on_click(self, pos: Tuple[int, int]):
        self.action(self)
