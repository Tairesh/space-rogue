from . import Sprite, octagon
from .buttons import Button
import pygame
import fonts
import colors
import engine
from time import time


class TextInput(Sprite):

    focusable = True
    focusable_view = True
    hoverable = True
    pressable = True
    disableable = True
    events_subscribe_on_focused = True

    def __init__(self, pos, value, width, groups=()):
        self.value = value
        self.width = width
        self.cursor_visible = False
        self.cursor_blinked_time = 0
        super().__init__(pos, groups)

    def _render_textinput(self, text_color, background_color, border) -> pygame.Surface:
        text_surface = fonts.mono.render(self.value, True, text_color)
        s = pygame.Surface((self.width, text_surface.get_height()+10), pygame.SRCALPHA, 32).convert_alpha()
        pol = octagon(s.get_width() - 1, s.get_height() - 1)
        pygame.draw.polygon(s, background_color, pol, 0)
        if border:
            pygame.draw.polygon(s, text_color, pol, border)
        s.blit(text_surface, (5, 5))
        if self.focused and self.cursor_visible:
            pygame.draw.rect(s, text_color, (text_surface.get_width()+7, 5, 10, text_surface.get_height()))
        return s

    def _render_state(self, state) -> pygame.Surface:
        if state == 'default':
            return self._render_textinput(colors.LightSpaceBlue, colors.Black, True)
        elif state == 'focused':
            return self._render_textinput(colors.White, colors.LightSpaceBlue, True)
        elif state == 'hovered':
            return self._render_textinput(colors.White, colors.LightSpaceBlue, False)
        elif state == 'pressed':
            return self._render_textinput(colors.White, colors.SpaceBlue, False)
        elif state == 'disabled':
            return self._render_textinput(colors.LightGray, colors.DarkGray, True)
        else:
            raise Exception(f"Unknown state `{state}` for class {self.__class__.__name__}")

    def update(self):
        if self.focused and time() - self.cursor_blinked_time > 0.7:
            self.cursor_visible = not self.cursor_visible
            self.cursor_blinked_time = time()
            self._re_render()
        super().update()

    def call(self, event: pygame.event.Event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_BACKSPACE:
                self.value = self.value[:-1:]
                self._re_render()
            elif event.key == pygame.K_ESCAPE:
                self.off_focused()
                engine.controller.focused_entity = None
            elif event.unicode and not event.mod & pygame.KMOD_CTRL and event.key != pygame.K_RETURN:
                self.value += event.unicode
                self._re_render()

    def on_focused(self):
        super().on_focused()
        self.cursor_visible = True
        self.cursor_blinked_time = time()
        self._re_render()

    def off_focused(self):
        super().off_focused()
        self.cursor_visible = False
        self.cursor_blinked_time = 0
        self._re_render()


class Selector(Sprite):

    focusable = True
    focusable_view = True
    hoverable = True
    disableable = True
    events_subscribe_on_focused = True

    def __init__(self, pos, values: dict, value: [str, int], width, groups=()):
        self.values = values
        self.value = value
        self.width = width
        super().__init__(pos, groups)

        prev_btn = Button((self.rect.left, self.rect.top, 'left', 'top'), '<', False, groups=groups,
                          action=lambda btn: self.prev())
        next_btn = Button((self.rect.right - prev_btn.rect.w, self.rect.top, 'left', 'top'), '>', False, groups=groups,
                          action=lambda btn: self.next())
        self.buttons = {
            'prev': prev_btn,
            'next': next_btn,
        }

    def _render_selector(self, text_color, background_color, border) -> pygame.Surface:
        text_surface = fonts.mono.render(self.values[self.value], True, text_color)
        s = pygame.Surface((self.width, text_surface.get_height()+10), pygame.SRCALPHA, 32).convert_alpha()
        pol = octagon(s.get_width() - 1, s.get_height() - 1)
        pygame.draw.polygon(s, background_color, pol, 0)
        if border:
            pygame.draw.polygon(s, text_color, pol, border)
        s.blit(text_surface, (s.get_width()//2-text_surface.get_width()//2, 5))
        return s

    def _render_state(self, state) -> pygame.Surface:
        if state == 'default':
            return self._render_selector(colors.LightSpaceBlue, colors.Black, True)
        elif state == 'focused':
            return self._render_selector(colors.White, colors.LightSpaceBlue, True)
        elif state == 'hovered':
            return self._render_selector(colors.White, colors.LightSpaceBlue, False)
        elif state == 'disabled':
            return self._render_selector(colors.LightGray, colors.DarkGray, True)
        else:
            raise Exception(f"Unknown state `{state}` for class {self.__class__.__name__}")

    def next(self):
        keys = list(self.values.keys())
        i = keys.index(self.value)
        i += 1
        if i >= len(keys):
            i = 0
        k = keys[i]
        self.set_value(k)

    def prev(self):
        keys = list(self.values.keys())
        i = keys.index(self.value)
        i -= 1
        if i < 0:
            i = len(keys)-1
        k = keys[i]
        self.set_value(k)

    def set_value(self, value):
        self.value = value
        self._re_render()
        # self.on_change()

    def call(self, event: pygame.event.Event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                self.prev()
                self.buttons['prev'].on_pressed()
            elif event.key == pygame.K_RIGHT:
                self.next()
                self.buttons['next'].on_pressed()
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                self.buttons['prev'].off_pressed()
            elif event.key == pygame.K_RIGHT:
                self.buttons['next'].off_pressed()

    def on_focused(self):
        super().on_focused()
        for btn in self.buttons.values():
            btn.on_focused()

    def off_focused(self):
        super().off_focused()
        for btn in self.buttons.values():
            btn.off_focused()
