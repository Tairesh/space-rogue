import pygame

mono: pygame.font.SysFont


def init():
    global mono
    pygame.font.init()
    mono = pygame.font.SysFont('Ubuntu Mono', 16, True)
