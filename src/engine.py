import pygame
import consts
import gamedata
import settings
import sizes
import tileset
import fonts
import colors
from controllers import ControllerView
from controllers.mainmenu import MainMenu


window: pygame.Surface
clock: pygame.time.Clock
controller: ControllerView


def window_mode():
    mode = pygame.HWSURFACE | pygame.DOUBLEBUF
    if settings.fullscreen:
        mode |= pygame.FULLSCREEN
    else:
        mode |= pygame.RESIZABLE
    return mode


def change_controller(new_controller):
    global controller
    controller = new_controller


def init():
    global window, clock, controller

    settings.init()
    gamedata.init()

    pygame.display.init()
    pygame.display.set_caption(consts.WINDOW_TITLE)
    pygame.display.set_icon(pygame.image.load(consts.WINDOW_ICON))
    window = pygame.display.set_mode((settings.width, settings.height), window_mode())
    clock = pygame.time.Clock()

    tileset.init()
    fonts.init()
    sizes.calc()

    controller = MainMenu()


def flush():
    window.fill(colors.Black)
    controller.draw(window)
    if settings.show_fps:
        fps = fonts.mono.render(str(int(clock.get_fps())), True, colors.Lime)
        window.blit(fps, (sizes.screen_width-fps.get_width()-10, 10))
    pygame.display.update()


def loop():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            elif event.type == pygame.VIDEORESIZE:
                pygame.display.set_mode(event.dict['size'], window_mode())
                sizes.calc()
                controller.on_resize()
            else:
                controller.call(event)
        controller.tick()
        flush()
        clock.tick(consts.FPS)
